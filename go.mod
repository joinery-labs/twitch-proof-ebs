module gitlab.com/shrmpy/twitch-proof-ebs

go 1.16

require (
	github.com/aws/aws-lambda-go v1.26.0
	github.com/awslabs/aws-lambda-go-api-proxy v0.11.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fauna/faunadb-go/v4 v4.1.0
	github.com/mailjet/mailjet-apiv3-go v0.0.0-20201009050126-c24bc15a9394
	github.com/stretchr/testify v1.6.1
)
